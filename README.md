# PPK-Praktikum 12 : RecyclerView

## Identitas

```
Nama : Novanni Indi Pradana
NIM  : 222011436
Kelas: 3SI3

```

## Deskripsi

Sebagaimana dijelaskan pada halaman web https://developer.android.com/guide/topics/ui/layout/recyclerview?hl=id RecyclerView sebagai alternatif dari ListView memudahkan untuk menampilkan kumpulan data dalam jumlah besar secara efisien. Anda menyediakan data dan menentukan tampilan setiap item, dan library RecyclerView secara dinamis membuat elemen saat diperlukan. 

Sesuai namanya, RecyclerView mendaur ulang elemen individual tersebut. Saat item di-scroll keluar layar, RecyclerView tidak merusak tampilannya. Sebaliknya, RecyclerView menggunakan kembali tampilan tersebut untuk item baru yang telah di-scroll di layar. Penggunaan ulang ini sangat meningkatkan performa, meningkatkan daya respons aplikasi, dan mengurangi pemakaian daya. 

Catatan: Selain menjadi nama class, RecyclerView juga merupakan nama library. Di halaman ini, RecyclerView di code font selalu berarti class dalam library RecyclerView.


## Kegiatan Praktikum

### Tampilan Data Mahasiswa
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-12/-/raw/main/Screenshot/Screenshot%20(1).png)
### Toast Ketika Salah Satu Mahasiswa di Klik
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-12/-/raw/main/Screenshot/Screenshot%20(2).png)

